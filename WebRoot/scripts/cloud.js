/**
 * 调用后台SpringMVC Action
 */
function _remoteCall(url, params, callback, isJson, isAsync) {
	$.ajax({
		type: "POST",
		url: top.basePath + url,
		data: params ? params : {},
		async: isAsync ? false : true,
		dataType: isJson ? "json" : "text",
		success: function(data) {
			if(typeof(callback) == "function")  callback(data);
		}
	});
}

/**
 * open wait loader
 * 
 * @param type
 */
function openLoader(type) {
	if(!type)  type = 1;
	$("#loaderTip", top.document).text($("#loaderTip" + type, top.document).text());
	
	$("#loader", top.document).show();
}

/**
 * close wait loader
 */
function closeLoader() {
	$("#loader", top.document).hide();
}

/**
 * iframe adjust height by page content
*/
function autoHeight() {
	var ifm = top.document.getElementById("mainFrame");
	var h = $("div.wrapper", ifm.contentDocument.body).outerHeight() + 20;
	var minH = top.document.body.clientHeight - 100;
	
	ifm.height = h < minH ? minH : h;
}

function autoFrameHeight($frm, resetScroll) {
	if(!$frm) $frm = $("iframe", parent.document);
	
	var h = $("div.wrapper", $frm[0].contentDocument.body).outerHeight();
	var minH = top.document.body.clientHeight - 140;
	
	$frm[0].height = h < minH ? minH : h;
	autoHeight();
	
	if(resetScroll)  top.document.body.scrollTop = 0;
}

function ppmAlert(title, content) {
	$("#dialog-alert > p", top.document).text(content);
	
	$("#dialog-alert", top.document).dialog({
		resizable: false,
	    height: 200,
	    modal: true,
	    title: title,
	    position: ["center", 200],
	    buttons: {
	        "确定": function() { $(this).dialog("close"); }
		}
	});
}

function ppmConfirm(title, content, func) {
	$("#confirm-content", top.document).text(content);
	
	$("#dialog-confirm", top.document).dialog({
		resizable: false,
	    height: 200,
	    modal: true,
	    title: title,
	    position: ["center", 200],
	    buttons: {
	        "确定": function() {
	        	if(func && typeof(func) == "function")  func();
	        	$(this).dialog("close");
	        },
	        "取消": function() { $(this).dialog("close"); }
		}
	});
}

/**
 * ****************************************************************************
 * select operates
 * ****************************************************************************
 */

/**
 * show select users dialog
 * 
 * @param $inp
 */
function showUser($inp) {
	loadUserData($inp);
	$("#userModal").modal("show");
}

/**
 * show select multi users dialog
 * 
 * @param $inp
 */
function showUsers($inp) {
	loadUsersData($inp);
	$("#usersModal").modal("show");
}

/**
 * show select project dialog
 * 
 * @param $inp
 */
function showProject($inp) {
	loadProjectData($inp);
	$("#projectModal").modal("show");
}

/**
 * show select multi projects dialog
 * 
 * @param $inp
 */
function showProjects($inp) {
	loadProjectData($inp);
	$("#projectsModal").modal("show");
}

/**
 * show select status dialog
 * 
 * @param $imp
 */
function showStatuses($inp) {
	initStatus($inp);
	$("#statusModal").modal("show");
}

/**
 * show select level dialog
 * 
 * @param $inp
 */
function showLevels($inp) {
	initLevel($inp);
	$("#levelModal").modal("show");
}

/**
 * show select priority dialog
 * 
 * @param $inp
 */
function showPriorities($inp) {
	initPriority($inp);
	$("#priorityModal").modal("show");
}

/**
 * show select department dialog
 * 
 * @param $inp
 */
function showDepartment($inp) {
	loadDepartData($inp);
	$("#departModal").modal("show");
}

/**
 * ****************************************************************************
 * date operates
 * ****************************************************************************
 */
function getDateStr(date) {
	if(!date)  return "";
	
	// if is java date json, convert to javascript date
	if(date.time)  date = new Date(date.time);
	
	return date.getFullYear() + "-" + formatTime(date.getMonth() + 1) + "-" + formatTime(date.getDate());
}

function getTimeStr(date, isSimple) {
	if(!date)  return "";
	
	// if is java date json, convert to javascript date
	if(date.time)  date = new Date(date.time);
	
	return (isSimple ? "" : (date.getFullYear() + "-")) + formatTime(date.getMonth() + 1) + "-" + formatTime(date.getDate())
			+ " " + formatTime(date.getHours()) + ":" + formatTime(date.getMinutes());
}

function formatTime(d) {
	if(d < 10)  return "0" + d;
	return d;
}

/**
 * ****************************************************************************
 * page operates
 * ****************************************************************************
 */
function initPage(page) {
	var html = "<li" + (page.page == 1 ? " class='disabled'" : "") + " onclick='gotoPage($(this), -1, " + page.page + ");'><a href='#' onclick='return false;'>上一页</a></li>";
	
	for(var i = 0; i < page.pageNum; i++) {
		html += "<li" + (page.page == (parseInt(i) + 1) ? " class='disabled'" : "") + " onclick='gotoPage($(this));'><a href='#' onclick='return false;'>" + (parseInt(i) + 1) + "</a></li>";
	}
	
	html += "<li" + (page.page == page.pageNum ? " class='disabled'" : "") + " onclick='gotoPage($(this), 1, " + page.page + ");'><a href='#' onclick='return false;'>下一页</a></li>";
	
	$("div.pagination ul").html(html);
}

function gotoPage($li, next, page) {
	if($li.hasClass("disabled"))  return;
	if(typeof(search) != "function")  return;
	
	if(!next) {
		search($li.text().trim());
	}
	else if(next < 0) {
		search(page - 1);
	}
	else if (next > 0) {
		search(page + 1);
	}
}

function getPageSn(page, i) {
	return ((page.page - 1) * page.pageSize + parseInt(i) + 1);
}