<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<div id="usersModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">选择用户</h3>
	</div>
	
	<div class="modal-body">
		<table id="usersTab" class="list-table">
			<tr>
				<th width="80px" style="padding-left: 20px;"><input id="u_all" name="_user" type="checkbox" /></th>
				<th width="310px">用户名</th>
				<th width="150px">职位</th>
			</tr>
		</table>
	</div>
	
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
		<button onclick="selectUsers(true);" class="btn">清空</button>
		<button onclick="selectUsers();" class="btn btn-primary">确认</button>
	</div>
</div>

<script>
	var $_us_src;
	
	function loadUsersData($inp) {
		$_us_src = $inp;
		var id = $inp.attr("val"), id = id ? id : "";
		
		_remoteCall("user/getUsers.do", {}, function(data) {
			var info = eval(data), html = "", $tab = $("#usersTab");
			
			// remove origin rows
			$("tr:gt(0)", $tab).remove();
			
			for(var i in info) {
				html += "<tr id='" + info[i].id + "' onclick='clickUsers($(this));'>";
				html += "<td style='padding-left: 20px;'><input name='_user' type='checkbox' " + (id.indexOf(info[i].id) >= 0 ? "checked" : "") + " /></td>";
				html += "<td><div>" + info[i].username + "</div></td>";
				html += "<td><div>" + combinePosition(info[i].type) + "</div></td>";
				html += "</tr>";
			}
			
			$tab.append(html);
			
			// init check box
			$("input").iCheck({checkboxClass: "icheckbox_square-blue"});
			
			// init select all
			$("#u_all").on("ifChecked", function() { $("#usersModal input").iCheck("check"); }).on("ifUnchecked", function() { $("#usersModal input").iCheck("uncheck"); });
		});
	}
	
	function clickUsers($tr) {
		$("input", $tr).iCheck("toggle");
	}
	
	function selectUsers(isClear) {
		if(isClear) {
			$_us_src.attr("val", "").val("");
		} else {
			var $sel = $("#usersTab input:checkbox:checked"), $tr, ids = [], names = [];
			
			$sel.each(function() {
				$tr = $(this).closest("tr");
				if(!$tr.attr("id"))  return true;
				
				ids.push($tr.attr("id"));
				names.push($("td:eq(1)", $tr).text());
			});
			
			$_us_src.attr("val", ids.join(",")).val(names.join(", "));
		}
		
		// if has filter, call search to refresh table
		if(typeof(search) == "function")  search();
		
		$("#usersModal").modal("hide");
	}
	
	function combinePosition(t) {
		if(t == 1) {
			return "项目经理";
		} else if(t == 2) {
			return "研发工程师";
		} else {
			return "测试工程师";
		}
	}
</script>
