<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<div id="priorityModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">选择优先级</h3>
	</div>
	
	<div class="modal-body">
		<table id="priorityTab" class="list-table">
			<tr>
				<th width="80px" style="padding-left: 20px;"><input id="pr_all" name="_user" type="checkbox" /></th>
				<th width="460px">优先级</th>
			</tr>
			<tr id="1" onclick="clickPriority($(this));">
				<td style="padding-left: 20px;"><input name="_status" type="checkbox" /></td>
				<td><div>高</div></td>
			</tr>
			<tr id="2" onclick="clickPriority($(this));">
				<td style="padding-left: 20px;"><input name="_status" type="checkbox" /></td>
				<td><div>中</div></td>
			</tr>
			<tr id="3" onclick="clickPriority($(this));">
				<td style="padding-left: 20px;"><input name="_status" type="checkbox" /></td>
				<td><div>低</div></td>
			</tr>
		</table>
	</div>
	
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
		<button onclick="selectPriority(true);" class="btn">清空</button>
		<button onclick="selectPriority();" class="btn btn-primary">确认</button>
	</div>
</div>

<script>
	var $_pr_src;
	
	function initPriority($inp) {
		$_pr_src = $inp;
		
		// init select statuses
		var id = $inp.attr("val");  id = id ? id : "";
		
		$("#priorityTab input:checkbox").iCheck("uncheck");
		
		$("#priorityTab tr:gt(0)").each(function() {
			if(id.indexOf($(this).attr("id")) >= 0)  $("input:checkbox", $(this)).iCheck("check");
		});
		
		// init check box
		$("input").iCheck({checkboxClass: "icheckbox_square-blue"});
		
		// init select all
		$("#pr_all").on("ifChecked", function() { $("#priorityModal input").iCheck("check"); }).on("ifUnchecked", function() { $("#priorityModal input").iCheck("uncheck"); });
	}
	
	function clickPriority($tr) {
		$("input", $tr).iCheck("toggle");
	}
	
	function selectPriority(isClear) {
		if(isClear) {
			$_pr_src.attr("val", "").val("");
		} else {
			var $sel = $("#priorityModal input:checkbox:checked"), $tr, ids = [], names = [];
			
			$sel.each(function() {
				$tr = $(this).closest("tr");
				if(!$tr.attr("id"))  return true;
				
				ids.push($tr.attr("id"));
				names.push($("td:eq(1)", $tr).text());
			});
			
			$_pr_src.attr("val", ids.join(",")).val(names.join(", "));
		}
		
		// if has filter, call search to refresh table
		if(typeof(search) == "function")  search();
		
		$("#priorityModal").modal("hide");
	}
</script>
