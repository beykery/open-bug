<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<div id="levelModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">选择严重性</h3>
	</div>
	
	<div class="modal-body">
		<table id="levelTab" class="list-table">
			<tr>
				<th width="80px" style="padding-left: 20px;"><input id="l_all" name="_user" type="checkbox" /></th>
				<th width="460px">严重性</th>
			</tr>
			<tr id="1" onclick="clickLevel($(this));">
				<td style="padding-left: 20px;"><input name="_status" type="checkbox" /></td>
				<td><div>致命</div></td>
			</tr>
			<tr id="2" onclick="clickLevel($(this));">
				<td style="padding-left: 20px;"><input name="_status" type="checkbox" /></td>
				<td><div>严重</div></td>
			</tr>
			<tr id="3" onclick="clickLevel($(this));">
				<td style="padding-left: 20px;"><input name="_status" type="checkbox" /></td>
				<td><div>一般</div></td>
			</tr>
			<tr id="4" onclick="clickLevel($(this));">
				<td style="padding-left: 20px;"><input name="_status" type="checkbox" /></td>
				<td><div>优化</div></td>
			</tr>
		</table>
	</div>
	
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
		<button onclick="selectLevel(true);" class="btn">清空</button>
		<button onclick="selectLevel();" class="btn btn-primary">确认</button>
	</div>
</div>

<script>
	var $_l_src;
	
	function initLevel($inp) {
		$_l_src = $inp;
		
		// init select statuses
		var id = $inp.attr("val");  id = id ? id : "";
		
		$("#levelTab input:checkbox").iCheck("uncheck");
		
		$("#levelTab tr:gt(0)").each(function() {
			if(id.indexOf($(this).attr("id")) >= 0)  $("input:checkbox", $(this)).iCheck("check");
		});
		
		// init check box
		$("input").iCheck({checkboxClass: "icheckbox_square-blue"});
		
		// init select all
		$("#l_all").on("ifChecked", function() { $("#levelModal input").iCheck("check"); }).on("ifUnchecked", function() { $("#levelModal input").iCheck("uncheck"); });
	}
	
	function clickLevel($tr) {
		$("input", $tr).iCheck("toggle");
	}
	
	function selectLevel(isClear) {
		if(isClear) {
			$_l_src.attr("val", "").val("");
		} else {
			var $sel = $("#levelModal input:checkbox:checked"), $tr, ids = [], names = [];
			
			$sel.each(function() {
				$tr = $(this).closest("tr");
				if(!$tr.attr("id"))  return true;
				
				ids.push($tr.attr("id"));
				names.push($("td:eq(1)", $tr).text());
			});
			
			$_l_src.attr("val", ids.join(",")).val(names.join(", "));
		}
		
		// if has filter, call search to refresh table
		if(typeof(search) == "function")  search();
		
		$("#levelModal").modal("hide");
	}
</script>
