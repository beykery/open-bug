<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<div id="departModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">选择部门</h3>
	</div>
	
	<div class="modal-body">
		<table id="departTab" class="list-table">
			<tr>
				<th width="80px"></th>
				<th width="310px">部门名称</th>
				<th width="150px">部门经理</th>
			</tr>
		</table>
	</div>
	
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
		<button onclick="selectDepart(true);" class="btn">清空</button>
		<button onclick="selectDepart();" class="btn btn-primary">确认</button>
	</div>
</div>

<script>
	var $_d_src;
	
	function loadDepartData($inp) {
		$_d_src = $inp;
		var id = $inp.attr("val"), id = id ? id : "";
		
		_remoteCall("user/getDeparts.do", null, function(data) {
			var info = eval(data), html = "", $tab = $("#departTab");
			
			// remove origin rows
			$("tr:gt(0)", $tab).remove();
			
			for(var i in info) {
				html += "<tr id='" + info[i].id + "' onclick='clickDepart($(this));' ondblclick='selectDepart();'>";
				html += "<td class='sn'><input name='_depart' type='radio' " + (id == info[i].id ? "checked" : "") + " /></td>";
				html += "<td><div>" + info[i].name + "</div></td>";
				html += "<td><div>" + info[i].manager + "</div></td>";
				html += "</tr>";
			}
			
			$tab.append(html);
			
			// init radio box
			$("input").iCheck({radioClass: "iradio_square-blue"});
		});
	}
	
	function clickDepart($tr) {
		$("input", $tr).iCheck("check");
	}
	
	function selectDepart(isClear) {
		if(isClear) {
			$_d_src.attr("val", "").val("");
		} else {
			var $sel = $("#departModal input:radio:checked").closest("tr");
			$_d_src.attr("val", $sel.attr("id")).val($("td:eq(1)", $sel).text());
		}
		
		// if has filter, call search to refresh table
		if(typeof(search) == "function")  search();
		
		$("#departModal").modal("hide");
	}
</script>
