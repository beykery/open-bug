<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<div id="projectsModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">选择项目</h3>
	</div>
	
	<div class="modal-body">
		<table id="projectsTab" class="list-table">
			<tr>
				<th width="80px" style="padding-left: 20px;"><input id="p_all" name="_user" type="checkbox" /></th>
				<th width="310px">项目名称</th>
				<th width="150px">项目经理</th>
			</tr>
		</table>
	</div>
	
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
		<button onclick="selectProject(true);" class="btn">清空</button>
		<button onclick="selectProject();" class="btn btn-primary">确认</button>
	</div>
</div>

<script>
	var $_p_src;
	
	function loadProjectData($inp) {
		$_p_src = $inp;
		var id = $inp.attr("val"), id = id ? id : "";
		
		_remoteCall("project/getProjects.do", null, function(data) {
			var d = eval("(" + data + ")"), info = d.projects, html = "", $tab = $("#projectsTab");
			
			// remove origin rows
			$("tr:gt(0)", $tab).remove();
			
			for(var i in info) {
				html += "<tr id='" + info[i].id + "' onclick='clickProject($(this));'>";
				html += "<td style='padding-left: 20px;'><input name='_user' type='checkbox' " + (id.indexOf(info[i].id) >= 0 ? "checked" : "") + " /></td>";
				html += "<td><div>" + info[i].name + "</div></td>";
				html += "<td><div>" + info[i].manager + "</div></td>";
				html += "</tr>";
			}
			
			$tab.append(html);
			
			// init check box
			$("input").iCheck({checkboxClass: "icheckbox_square-blue"});
			
			// init select all
			$("#p_all").on("ifChecked", function() { $("#projectsModal input").iCheck("check"); }).on("ifUnchecked", function() { $("#projectsModal input").iCheck("uncheck"); });
		});
	}
	
	function clickProject($tr) {
		$("input", $tr).iCheck("toggle");
	}
	
	function selectProject(isClear) {
		if(isClear) {
			$_p_src.attr("val", "").val("");
		} else {
			var $sel = $("#projectsTab input:checkbox:checked"), $tr, ids = [], names = [];
			
			$sel.each(function() {
				$tr = $(this).closest("tr");
				if(!$tr.attr("id"))  return true;
				
				ids.push($tr.attr("id"));
				names.push($("td:eq(1)", $tr).text());
			});
			
			$_p_src.attr("val", ids.join(",")).val(names.join(", "));
		}
		
		// if has filter, call search to refresh table
		if(typeof(search) == "function")  search();
		
		$("#projectsModal").modal("hide");
	}
</script>
