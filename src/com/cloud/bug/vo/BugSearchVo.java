package com.cloud.bug.vo;


public class BugSearchVo {

	/**
	 * page
	 */
	private int page = 1;
	private int pageSize = 15;
	private int pageNum = 1;
	
	/**
	 * sort condition
	 */
	private String sort;
	
	/**
	 * filter condition
	 */
	private String code;
	private String name;
	private String projectIds;
	private String status;
	private String ownerIds;
	private String levels;
	private String priorities;
	
	/**
	 * if from workspace
	 */
	private String workspace;
	
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getProjectIds() {
		return projectIds;
	}
	
	public void setProjectIds(String projectIds) {
		this.projectIds = projectIds;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getOwnerIds() {
		return ownerIds;
	}
	
	public void setOwnerIds(String ownerIds) {
		this.ownerIds = ownerIds;
	}
	
	public String getLevels() {
		return levels;
	}

	public void setLevels(String levels) {
		this.levels = levels;
	}

	public String getPriorities() {
		return priorities;
	}

	public void setPriorities(String priorities) {
		this.priorities = priorities;
	}
	
	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}
	
	public String getWorkspace() {
		return workspace;
	}

	public void setWorkspace(String workspace) {
		this.workspace = workspace;
	}
}
