package com.cloud.entity;

public class EntityUtil {
	
	public static final String ENTITY_ID = "entityId";

	public static final String OPERATE_ADD = "add";
	public static final String OPERATE_EDIT = "edit";
	public static final String OPERATE_LIST = "list";
	public static final String OPERATE_SAVE = "save";
	public static final String OPERATE_REMOVE = "remove";
}
