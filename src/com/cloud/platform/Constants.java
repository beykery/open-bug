package com.cloud.platform;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.cloud.bug.util.BugPageUtil;
import com.cloud.project.model.Project;
import com.cloud.security.model.Department;
import com.cloud.security.model.User;
import com.cloud.security.service.UserService;
import com.cloud.system.model.SystemConfig;

public class Constants {

	public static final String VALID_YES = "Y";
	public static final String VALID_NO = "N";
	
	public static final String BASEPATH = "/open-bug/";
	public static String ROOTPATH = "";
	
	public static SystemConfig systemConfig;
	
	/*
	 * ================== bug status ==================
	 */
	public static final int BUG_STATUS_START = 0;
	public static final int BUG_STATUS_INIT = 1;
	public static final int BUG_STATUS_AUDIT = 2;
	public static final int BUG_STATUS_SOLVE = 3;
	public static final int BUG_STATUS_TEST = 4;
	public static final int BUG_STATUS_HANGUP = 5;
	public static final int BUG_STATUS_CLOSE = 6;
	
	/*
	 * ================== name id map ==================
	 */
	public static Map<String, String> userNameIdMap;
	public static Map<String, String> userIdNameMap;
	public static Map<String, String> projectNameIdMap;
	public static Map<String, String> projectIdNameMap;
	public static Map<String, Department> departTreeMap;
	
	public static String getID() {
		return UUID.randomUUID().toString();
	}
	
	/**
	 * get system config
	 * 
	 * @return
	 */
	public static SystemConfig getSystemConfig() {
		
		if(systemConfig == null) {
			IDao dao = (IDao) SpringUtil.getBean("dao");
			List<SystemConfig> list = dao.getAllByHql("from SystemConfig");
			
			systemConfig = list.get(0);
		}
		
		return systemConfig;
	}
	
	/**
	 * get operate name by operate string
	 * 
	 * @param op
	 * @return
	 */
	public static String getOperateName(String op) {
		String opName = "";
		
		if(BugPageUtil.OP_CREATE.equals(op)) {
			opName = "创建";
		}
		else if(BugPageUtil.OP_EDIT.equals(op)) {
			opName = "编辑";
		}
		else if(BugPageUtil.OP_COMMIT.equals(op)) {
			opName = "提交审核";
		}
		else if(BugPageUtil.OP_ASSIGN.equals(op)) {
			opName = "指定修改";
		}
		else if(BugPageUtil.OP_UNPASS.equals(op)) {
			opName = "审核不通过";
		}
		else if(BugPageUtil.OP_HANGUP.equals(op)) {
			opName = "挂起";
		}
		else if(BugPageUtil.OP_CLOSE.equals(op)) {
			opName = "直接关闭";
		}
		else if(BugPageUtil.OP_GOTEST.equals(op)) {
			opName = "提交测试";
		}
		else if(BugPageUtil.OP_SUCCESS.equals(op)) {
			opName = "测试通过";
		}
		else if(BugPageUtil.OP_FAILURE.equals(op)) {
			opName = "测试不通过";
		}
		else if(BugPageUtil.OP_CHGTESTOR.equals(op)) {
			opName = "更换责任人";
		}
		else if(BugPageUtil.OP_REOPEN.equals(op)) {
			opName = "重新开启";
		}
		
		return opName;
	}
	
	/**
	 * check if is image
	 * 
	 * @param type
	 * @return
	 */
	public static boolean isImage(String type) {
		type = type.toUpperCase();
		
		return type.equals("JPG") || type.equals("JPEG") || type.equals("GIF")
				|| type.equals("PNG") || type.equals("BMP");
	}
	
	/**
	 * ========================= project operate =========================
	 */
	public static String getProjectNameById(String projectId) {
		
		if(projectNameIdMap == null) {
			projectNameIdMap = new HashMap();
			
			IDao dao = (IDao) SpringUtil.getBean("dao");
			List<Project> projects = dao.getAllByHql("from Project");
			
			for(Project p : projects) {
				projectNameIdMap.put(p.getId(), p.getName());
			}
		}
		
		if(!projectNameIdMap.containsKey(projectId)) {
			return "";
		}
		
		return projectNameIdMap.get(projectId);
	}
	
	public static String getProjectIdByName(String projectName) {
		
		if(projectIdNameMap == null) {
			projectIdNameMap = new HashMap();
			
			IDao dao = (IDao) SpringUtil.getBean("dao");
			List<Project> projects = dao.getAllByHql("from Project");
			
			for(Project p : projects) {
				projectIdNameMap.put(p.getName(), p.getId());
			}
		}
		
		if(!projectIdNameMap.containsKey(projectName)) {
			return "";
		}
		
		return projectIdNameMap.get(projectName);
	}
	
	/**
	 * ========================= user operate =========================
	 */
	public static String getLoginUserId() {
		
		UserDetails userDetails = (UserDetails) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();
		
		String userId = getUserNameToIdMap().get(userDetails.getUsername());
		
		return StringUtil.isNullOrEmpty(userId) ? "" : userId;
	}
	
	public static boolean isAdmin() {
		
		Object userDetails = SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();
		
		if(!(userDetails instanceof UserDetails)) {
			return false;
		}
		
		return "admin".equals(((UserDetails) userDetails).getUsername());
	}

	public static String getUsernameById(String userId) {
		if(StringUtil.isNullOrEmpty(userId)) {
			return "";
		}
		
		return getUserIdToNameMap().get(userId);
	}
	
	public static String getUserIdByName(String username) {
		if(StringUtil.isNullOrEmpty(username)) {
			return "";
		}
		
		return getUserIdByName(username);
	}
	
	public static Map<String, String> getUserNameToIdMap() {
		
		if(userNameIdMap == null) {
			userNameIdMap = new HashMap();
			
			UserService userSerivce = (UserService) SpringUtil.getBean("userService");
			List<User> users = userSerivce.searchUsers();
			
			for(User u : users) {
				userNameIdMap.put(u.getUsername(), u.getId());
			}
			
			userNameIdMap.put("admin", "000000000000000000000000000000000000");
		}
		
		return userNameIdMap;
	}
	
	public static Map<String, String> getUserIdToNameMap() {
		
		if(userIdNameMap == null) {
			userIdNameMap = new HashMap();
			
			UserService userSerivce = (UserService) SpringUtil.getBean("userService");
			List<User> users = userSerivce.searchUsers();
			
			for(User u : users) {
				userIdNameMap.put(u.getId(), u.getUsername());
			}
			
			userIdNameMap.put("000000000000000000000000000000000000", "admin");
		}
		
		return userIdNameMap;
	}
	
	/**
	 * ========================= department operate =========================
	 */
	public static Map<String, Department> getDepartTreeMap() {
		
		if(departTreeMap == null) {
			departTreeMap = new HashMap();
			
			// get all departments
			IDao dao = (IDao) SpringUtil.getBean("dao");
			List<Department> departs = dao.getAllByHql("from Department");
			
			// sort departments
			List<Department> sorted = TreeUtil.sortTree(departs);
			
			// get user department map
			UserService userSerivce = (UserService) SpringUtil.getBean("userService");
			List<User> users = userSerivce.searchUsers();
			Set<String> userDepartSet = new HashSet();
			
			for(User u : users) {
				if(!StringUtil.isNullOrEmpty(u.getDepartmentId())) {
					userDepartSet.add(u.getDepartmentId());
				}
			}
			
			// add department
			for(Department d : sorted) {
				if(userDepartSet.contains(d.getId())) {
					d.setHasChild(true);
				}
				
				departTreeMap.put(d.getId(), d);
			}
		}
		
		return departTreeMap;
	}
} 
