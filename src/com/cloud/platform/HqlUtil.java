package com.cloud.platform;

import java.util.Set;

public class HqlUtil {
	
	/**
	 * combine hql in string
	 * 
	 * @param strs
	 * @param isString
	 * @return
	 */
	public static String combineInHql(String field, Set<String> strSet, boolean isString) {
		
		if(strSet == null || strSet.size() == 0) {
			return "";
		}
		
		StringBuffer strs = new StringBuffer();
		
		for(String s : strSet) {
			strs.append(s + ",");
		}
		strs.deleteCharAt(strs.length() - 1);
		
		return combineInHql(field, strs.toString(), isString);
	}

	/**
	 * combine hql in string
	 * 
	 * @param strs
	 * @return
	 */
	public static String combineInHql(String field, String strs, boolean isString) {
		
		if(StringUtil.isNullOrEmpty(strs)) {
			return "";
		}
		
		StringBuffer sb = new StringBuffer(" " + field + " in (");
		
		for(String s : strs.split(",")) {
			
			if(StringUtil.isNullOrEmpty(s)) {
				continue;
			}
			
			if(isString) {
				sb.append("'" + s + "'");
			} else {
				sb.append(s);
			}
			
			sb.append(",");
		}
		
		sb.deleteCharAt(sb.length() - 1);
		sb.append(")");
		
		return sb.toString();
	}
}
