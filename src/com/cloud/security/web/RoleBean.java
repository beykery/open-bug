package com.cloud.security.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloud.bug.model.Bug;
import com.cloud.bug.service.BugService;
import com.cloud.platform.Constants;
import com.cloud.platform.SpringUtil;
import com.cloud.security.model.Resource;
import com.cloud.security.service.RoleService;
import com.cloud.security.util.RoleResUtil;

@Controller
@RequestMapping("role")
public class RoleBean {

	@Autowired
	private RoleService roleService;
	
	/**
	 * has structure manage authority
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/hasStrutManageAuth.do")
	public String hasStrutManageAuth() {
		
		return roleService.hasOperateAuth(RoleResUtil.RES_OP_STRUCTURE_MANAGE) ? Constants.VALID_YES
				: Constants.VALID_NO;
	}
	
	/**
	 * has bug all operate authority
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/hasBugAllOpAuth.do")
	public String hasBugAllOpAuth(@RequestParam("bugId") String bugId) {
		
		// check if is owner
		boolean isOwner = false;
		
		BugService bugService = (BugService) SpringUtil.getBean("bugService");
		Bug bug = bugService.getBug(bugId);
		
		if(Constants.getLoginUserId().equals(bug.getOwnerId())) {
			isOwner = true;
		}
		
		// check role authority
		boolean roleAuth = roleService.hasOperateAuth(RoleResUtil.RES_OP_BUG_ALL_OPERATE);
		
		return (roleAuth || isOwner) ? Constants.VALID_YES : Constants.VALID_NO;
	}
	
	/**
	 * has bug remove authority
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/hasBugRemoveAuth.do")
	public String hasBugRemoveAuth() {
		
		return roleService.hasOperateAuth(RoleResUtil.RES_OP_BUG_REMOVE) ? Constants.VALID_YES
				: Constants.VALID_NO;
	}
	
	/**
	 * has project manage authority
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/hasPjtManageAuth.do")
	public String hasPjtManageAuth() {
		
		return roleService.hasOperateAuth(RoleResUtil.RES_OP_PROJECT_MANAGE) ? Constants.VALID_YES
				: Constants.VALID_NO;
	}
	
	/**
	 * get resources to show in role form page
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getResources.do")
	public String getResources() {
		
		// search resources
		List<Resource> resources = roleService.searchResources();
		
		// format resource data
		Map<String, List<Resource>> result = new HashMap();
		
		for(Resource r : resources) {
			if(!result.containsKey(r.getCatStr())) {
				result.put(r.getCatStr(), new ArrayList());
			}
			
			result.get(r.getCatStr()).add(r);
		}
		
		return JSONObject.fromObject(result).toString();
	}
}
