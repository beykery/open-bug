package com.cloud.security.util;

public class RoleResUtil {

	public static final String RES_TYPE_URL = "URL";
	public static final String RES_TYPE_OP = "OP";
	
	public static final String RES_CAT_PJT = "PJT";
	public static final String RES_CAT_BUG = "BUG";
	public static final String RES_CAT_ORG = "ORG";
	public static final String RES_CAT_SYS = "SYS";
	
	public static final String RES_OP_PROJECT_MANAGE = "pjt_manage";
	public static final String RES_OP_BUG_REMOVE = "bug_remove";
	public static final String RES_OP_BUG_ALL_OPERATE = "bug_allop";
	public static final String RES_OP_STRUCTURE_MANAGE = "org_userdepart";
}
