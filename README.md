ppm-bug 缺陷管理系统
=======

项目主页： http://www.ppm123.cn

免费下载： http://ppm123.cn/pages/bug/detail.php

简介：

PPM缺陷管理系统是在见到当前免费的缺陷管理工具功能和界面都非常粗糙的情况下，旨在打造一个功能全面，界面清新的开源缺陷管理系统！

1. 工作面板 -- 快捷处理我接收的项目、缺陷，展示统计图和系统动态

2. 缺陷管理 -- 行业内领先缺陷管理流程，提高缺陷处理，管理效率

3. 项目状态 -- 按项目管理缺陷，直观统计项目下人员与缺陷的各种状态

4. 缺陷跟踪 -- 记录缺陷操作记录，流程图高亮显示操作路径与当前所处状态

5. 视图机制 -- 用户可在缺陷列表和工作面板中定制想看到的缺陷列表

6. 经验分享 -- 缺陷经验分享，跟其他研发人员分享缺陷解决的经验

7. 定制页面 -- 用户可自定义缺陷操作与查看页面的表单字段


版本：

PPM Bug v1.4 是PPM缺陷管理系统于2013年8月6号发布的第五个版本。

PPM Bug v1.4 特性：

1. 重构用户模块，引入组织架构，支持任意级部门与子部门

2. 重构职位模块，预置项目经理，研发工程师，测试工程师职位，用户可再自定义职位

3. 引入角色与权限系统，定制权限角色，用户可配置多个角色取权限并集

4. 对用户数据库中保存的密码进行不可解密式加密

5. 修改PPM Bug系统一些小Bug
 
技术体系
=======

前端：

主 -- JQuery + Bootstrap + JQueryUI(bootstrap theme) + iCheck + uploadify + FancyBox + HignCharts + SVG

附 -- JQuery自定义插件 + 下拉框插件 + Bootstrap TAB插件 + 表单校验插件

后端：

主 -- SpringMVC + Sitemesh + Hibernate(注解) + Spring(Ioc) + Spring Security + JSP2 TagDir + POI + log4j

附 -- 反射 + 自定义表单 + 实体增删改查自动处理框架

源码使用
=======

查看PPM Bug的源码并运行起来十分的简单，并且源码提供了非常详细的注释

1. 下载zip包，解压

2. Eclipse引入已存在项目，选择刚才解压的文件夹

3. PPM Bug采用内存数据库derby，所以只需将解压的文件夹下的db-bug文件夹放到tomcat安装目录的bin文件夹下即可，无需安装数据库

4. 打开WebRoot/WEB-INF/applicationContext.xml，将数据源中的jdbc:derby:D:/derby/bin/db-bug;改为jdbc:derby:db-bug;

5. 部署，运行，默认用户名admin，密码1
